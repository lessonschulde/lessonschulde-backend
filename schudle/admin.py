from django.contrib import admin
from .models import Group
from .models import AudienceType
from .models import Audience
# Register your models here.

admin.site.register(Group)
admin.site.register(AudienceType)
admin.site.register(Audience)
