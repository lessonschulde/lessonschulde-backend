from django.db import models

class Group(models.Model):

    name = models.CharField(max_length=64,blank=True, null=False, unique=True,default='')
    speciality = models.CharField(max_length=64, blank=True, null=False, unique=True, default='')
    count_people = models.IntegerField()

    class Meta:
        verbose_name = "Group"
        verbose_name_plural = "Groups"

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("Group_detail", kwargs={"pk": self.pk})

class AudienceType(models.Model):

    typeName = models.CharField(max_length=64,blank=True, null=False, unique=True,default='')

    class Meta:
        verbose_name = "AudienceType"
        verbose_name_plural = "AudienceTypes"

    def __str__(self):
        return self.typeName

    def get_absolute_url(self):
        return reverse("AudienceType_detail", kwargs={"pk": self.pk})


class Audience(models.Model):

    name = models.CharField(max_length=64,blank=True, null=False, unique=True,default='')
    locale = models.CharField(max_length=64,blank=True, null=False, unique=True,default='')
    responsible = models.CharField(max_length=150,blank=True, null=False, unique=True,default='')
    audienceType = models.ForeignKey(AudienceType, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Audience"
        verbose_name_plural = "Audiences"

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("Audience_detail", kwargs={"pk": self.pk})
